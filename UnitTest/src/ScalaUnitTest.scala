import org.joda.time.DateTime

object Test {

  def main(args: Array[String]) {

    //Funcion anonima Scala
    var y = (x: Int) => x - 3
    // Llamado a funcion anonima
    var j = y(8) - 3

    // esta funci�n puede retorna o no un entero.
    def z(paramFunction: => Int) = {
      println("Hola *2*", paramFunction);
      
    }

    println("<<<<",z(333))
    
    def paramFunction(a: Int) = {
      println("hfdsnf")

    }
    
    
// la funci�n "proceed" recibe una funci�n"timeCalc()" � "abc()" como par�metro
    proceed(timeCalc(5), "texto1")
    proceed(abc(4), "texto5")

    def timeCalc(a: Int) = {

      val r: Int = 2
      val s: String = "Algo"
      println(System.currentTimeMillis())
      System.currentTimeMillis()
      System.currentTimeMillis()
      //s
      ((r + a), s)
    }

    def abc(a: Int) = {
      (222 + a, "nada")
    }

    def proceed(t: => (Int, String), v: String) = {
      println("hola2")
      println("t es: " + t)
      t
      t
      println("t es: " + t)
    }

    def h(a: Int => Int, v: Int) = a(v)

    def suma(sum: Int) = {
      sum + 9

    }
    def resta(sum: Int) = {
      sum - 9
    }

    val div = h(suma, 5) / 7
    println("el valor de div es: " + div)

    object unObjeto {
      val myList = List(4, 9, 10, 11, 12, 13, 14)
    }
    // en Scala. Lambda function, toma un entero y entrega un "Unit"
    // (0 to 9).forEach(i => println(i))
    //val nuevaLista= (unObjeto => indice, suma(indice))

    val log = transmisionMensaje(new DateTime, _: String)
    log("el mensaje ue le quiero transmitir")
    log("Favor Validar")
    log("Favor Proceder")
    transmisionMensaje(new DateTime, "Favor Proceder")

    def xyz(ll: Int) = {
      ll * 100
    }
    val num1 = 55
    val num = 90

    println(suma2(xyz(3))(num))
    def suma2(a: => Int)(b: Int) = {
      a + b
    }

    //Listas Uniformes:
    val rest0 = List("aa", "bb", "cc", "dd")
    rest0.map { x => if (x != "aa") x.concat("s") }.foreach(println)

    //Funci*o*n en linea
    val juanPa = rest0.mkString("X")
    println("JuanPa es: " + juanPa)

    // Propiedades de listas      
    val ints = List(1, 2, 3, 4)
    val addOne = for (i <- ints) yield i + 1
    println("Retorna toda la lista con yield", addOne)
    val addOne2 = ints.map { x => x + 1 }
    println("Esta es mejor que la anterior ", addOne2)

    // Aplicar propiedades a cada elemento de la lista (flatted, aplanado)
    val ls = List("hello", "world", "howdy")
    println("Esta es mejor que la anterior ", ls.flatMap { x => List(x.concat("!")) })
    println("cambia el tipo de transformacion ", ls.map { x => List(x + "!") })

    // filtro, utilizado para subset de datos, solo los elementos que cumplen con el critero
    println(ls.filter { x => x.startsWith("h") })

    val nums = (0 to 100).toList
    // solo los numeros divisibles por 7
    println("Divisibles por 7: ", nums.filter { x => x % 7 == 0 })
    // numeros divisibles por 7 y por 3
    println("Divisibles por 7 y 3: ", nums.filter { x => x % 7 == 0 }.filter { x => x % 3 == 0 })
    // Todo en una linea
    println("Divisibles por 7 y 3 en una linea: ", (0 to 100).toList.filter { x => x % 7 == 0 }.filter { x => x % 3 == 0 })
    // Ordenamiento 
    val emps = List(Employee("John", 22), Employee("Alex", 53), Employee("Smith", 37))
    println(emps.sortBy(_.name)) // Equivale a x => x.name

    // The foldLeft operation applies the function passed to every element from left to right
    // def foldLeft[B](z: B)(f: (B, A) => B): B
    // foldLeft toma dos conuntos de par*a*metros.  acepta un valor inicial denotado por el par*a*metro "z",
    // del tipo B, y un par*a*metro de entrada que es una funci*o*n denotada por la funci*o*n "f". La funci*o*n "f"
    // acepta par*a*metros de tipo B y A, y salida B. Por *u*ltimo foldLeft retorna B. Aunque esto 
    // puede sonar muy oscuro en su representaci*o*n, debe ser poco intuitivo con los siguientes ejemplos:
    println(ls.foldLeft("*")((a, b) => a + b)) // aqu*i* "a" es el acumulador y "b" es el elemento
    println(emps.foldLeft("-")((a, b) => a + b)) // aqu*i* "a" es el acumulador y "b" es el elemento

    //*Futures* en escala, para manejar programaci*o*n concurrente.
    // Los *Futures* est*a*n escencialmente contenidos en un tipo de dato como: List, Map, Set y otros.
    // Estos contienen el valor de un c*a*lculo podr*i*a suceder en un punto despues en el tiempo. Mientras tanto,
    // cuando un *Futuro* es creado, el calculo que est*a* encerrado es ejecutado en un *ExcecutionContext*,
    // el cu*a*l es pasado como un par*a*metro impl*i*cito. El lenguaje Scala est*a* empaquetado con un *ExcecutionContext*
    // por defecto y brinda flexibilidad para escribir el nuestro.
    // El significado de un contexto de ejecuci*o*n es que elige un hilo libre del pool com*u*n 
    // de hilos dentro del *ExcecutionContext*. El hilo obtenido del pool ejecuta en el futuro, y cuando 
    // ha terminado, este notifica de regreso al c*o*digo que lo invoc*o*.
    // 

  }

  // Otra funci*o*n
  def transmisionMensaje(d: DateTime, a: String) = {
    println(a + d)
  }

}