import akka.actor.Actor

class RobotAkkaScala extends Actor {
  var moviendo: Boolean = false
  sealed abstract class Direction
  var direction: Direction = FORWARD
  case object FORWARD extends Direction
  case object BACKWARDS extends Direction
  case object RIGHT extends Direction
  case object LEFT extends Direction
  case class Move(direction: Direction)
  case object Stop
  case object GetRobotState
  case class RobotState(direction: Direction, moving: Boolean)

  //cuando extendemos de actor, debemos redefinir el metodo receive
  def receive = {
    case "tarea1" =>
      println("Tarea1. Mensaje de respuesta desde el actor, su estado es: ", moviendo)
      moviendo = true;
    case "tarea2" =>
      println("Tarea2.a. Mensaje de respuesta en el actor. antes:", moviendo)
      moviendo = false
      println("Tarea2.b.  de respuesta en el actor", moviendo)
    case "tarea3" =>
      println("Tarea3. Otra tarea")
    case Employee(a, b) =>
      if (a.equals("juan"))
        println("El desarrollador es jp")
      else
        println("El desarrollador no es jp")
    case Move(newDirection) =>
      moviendo = true
      direction = newDirection
      println(s"I am now moving $direction")
    case Stop =>
      moviendo = false
      println(s"I stopped moving")
    case msg => unhandled(msg)
  }

}