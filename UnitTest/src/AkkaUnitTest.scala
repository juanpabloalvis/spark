
import akka.actor.{ ActorSystem, Props }
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import akka.actor.Inbox
import scala.concurrent.duration._


object SparkAkkaTest extends App {
  
    // Creamos el sistema de actores
    val sistemaDeActores = ActorSystem("sistemaActoresRobot")

    // creamos un actor, y le ponemos el nombre. Pero en realidad esto es una referencia hacia el actor
    val robotAkka = sistemaDeActores.actorOf(Props[RobotAkkaScala],  name ="robotAkka")
    println("El Sistema actor \"ScalaRobotMain\" fue creado")

    // Una vez creado el sistema de actores, vamos a crear una instancia del actor:
    // El actor NO SE PUEDE CREAR de la forma normal utilizando new. Para crearlo se debe
    // hacer por medio de la fabrica, en este caso "ActorSystem", lo que es retornado 
    // no es una instancia como tal, sino una referencia "ActorRef" que apunta a nuestra instancia actor.
    // La fabrica en Akka que crea actores es el "ActorSystem" y tambien actua como contenedor de los actores
    // gestionando sus ciclos de vida. El actor se crea por medio del m*e*todo de la fabrica:
    // "actorOf". Este metodo toma la configuracion llamada "Props" y el nombre. Los nombres 
    // del actor (y del sistema de actores) son importantes, los utilizamos para buscar actores y
    // configurarlos en el archivo de configuracion.
    // El "ActorSystem" tambion gestiona el ciclo de vida del actor, gestiona el contexto de ejecucion(trhead pool)
    // en el cual los actores se ejecutan, un servicio de programacion, un flujo de eventos de lo que esta ocurriendo y mas.
    // El "ActorSystem" crea un actor en el top de la jerarquia. Los actores tambien pueden ser creados 
    // como hijos de otros actores utilizando un "Actor Context" local. El "Actor Context" contiene informacion
    // relevante sobre sobre el Sistema de Actores relevante a cada acrtor, como quien es el actor padre y quienes son los actor hijos.
    // Cuando un actor utiliza su contexto para crear otro actor, el nuevo actor llega a ser un actor hijo.
    // De esta manera es como se crea la jerarquia

    // Un actor no tiene un API publica en terminos de methodos que podamos invocar. El API de mensajes que el actor
    // maneja. Los mensajes pueden ser de cualquier tipo, objetos en Java o Scala.
    // En la clase roboot vamos a definir tres mensajes: {Moverse, Detenerse, ObtenerEstadoRobot, EstadoRobot}
    // Es importante que los mensajes que creamos sean inmutables(En java se utilizando clases est*e*ticas)
    // En scala, case clases y case objects son inmutables y tienen soporte para coincidencia de patrones

    // La comunicacion entre los actores se hace a travez de mensajeria asincronica. Esto hace a los actores reactivos.
    // Un actor no hace nada a menos que se le diga que haga algo, y se le dice que haga algo enviando un mensaje. Un mensaje 
    // asincr*e*nico significa que el emisor no espera a la respuesta del mensaje a que sea procesada por el receptor. 
    // A cambio el actor, maneja el mensaje fuera, poniendolo en el buzon del destinatario, y luego es libre para hacer algo 
    // mas importante que esperar por la reaccion del emisor sobre el mensaje. El buzon de correo del actor es escencialmente una
    // cola de mensajes y tiene semanticas de ordenamiento. Esto garantiza que el orden de multiples mensajes enviados 
    // desde el mismo actor son preservados. Mientras que los mismos mensajes pueden ser intercalados con mensajes envaidos por otro 
    // actor.
    // Cuando un actor no esta procesando mensajes, este se encuentra en un estado suspendido, el cual no consume ningun recurso 
    // distinto a la memoria.
    // se le dice al actor que haga algo, enviandole un mensaje en el metodo "tell" del ActorRef. Este metodo pone el mensaje en el
    // buzon de correo y retorna inmediatamente.

    // Internamente el actor responde a los mensajes sobreescribiendo el metodo "recibe". El metodo toma un simple parametro, un mensaje
    // del tipo "Any". En el metodo "recibe" el comportamiento principal del actor es definido. este comportamiento puede ser logica estandar,
    // como modificar el estado interno del actor, creando o llamando otros actores, logica de negocio o cualquier otro comportamiento.

    // Definamos los mensajes "Move" y "Stop" en el metodo "recibe" en la clase "RobotAkkaScala"

    
    // vamos a probar nuestros Actores enviando mensajes. Podemos utilizar el operador "Bang Operator" que dispara:
    robotAkka ! "tarea1"
    robotAkka ! "tarea2"
    robotAkka ! "tarea3"
    val empleado = Employee("juan", 22)
    robotAkka ! empleado
    
    val actorSpark = sistemaDeActores.actorOf(Props[ActorReadFile],  name ="actorSpark")
    actorSpark ! "tSpark1"
    actorSpark ! "tSpark2"
    
    
    // En algunos casos es inevitable hacer operaciones de bloqueo, por ejemplo una consulta a un RDBMS 
    // legado, esperar por un evento de red, o esperar desde un API de mensaeria.  Cuando enfrentamos esto, 
    // podriamos estar tentados a envolver esta llamada bloqueante dentro de un *Futuro* en vez de trabajar 
    // -con estos bloqueantes-. Esta estrategia es demaciado simple, pero es muy probable hallar cuellos de 
    // botella o "run out memory" cuando la aplicacion corre bajo carga.
    // La lista no exaustiva de soluciones adecuadas al "problema del bloqueo" incluye las siguentes sugerencias:

    //  *    Realice la llamada bloqueante dentro del actor(o un conjunto gestionados por un router -Scala-, asegurandose de configurar) 
    //       un pool de hilos el cual es dedicado para este proposito suficientemente dimemsionada.
    //  *    Realice la llamada bloqueante dentro de un *Future*, asegurando un limite superior sobre el numero de 
    //       dichas llamadas en cualquier punto del tiempo. (Enviando un numero ilimitado de tareas de esta naturaleza, podria agotar la memoria o el limite de hilos.
    //  *    Realice la llamada bloqueante dentro de un *Future*, ofreciendo un pool de hilos dentro de un limite
    //       de hilos apropiado para el hardware en el cual se ejecuta la aplicacion.
    //  *    Dedicar un simple hilo para gestionar un conjunto de recursos bloqueantes, y despachando eventos a medida de 
    //       que ocurre mensaje de actores.
    // La primera posibilidad es especialmente bien situada, para recursos los cuales son de naturaleza monohilo, como manejadores
    // de base de datos tradicionales, los cuales tradicionamente pueden ejecutar una consulta destacada a la vez y utilizar sincronizacion interna
    // para aseguar esto. Un patron comun para crear esto es un router para *N* actores, cada uno envuelve una simple conexi*o*n a una BD, y
    // maneja consultas para enviar hacia el router.
    // El numero *N* debe ser luego tuneado para maximo desempe*n*o, el cual podria variar dependiendo en cual DBMS esta desplegado sobre que hardware.
    // http://doc.akka.io/docs/akka/snapshot/general/actor-systems.html#What_you_should_not_concern_yourself_with
    
    // Los actores tiene un ciclo de vida especifico, y no se destruyen automaticamente cuando se dejan desreferenciar, despues de haber creado uno,
    // es nuestra responsabilidad asegurarnos que eventualmente sera terminado tambien-lo que nos da el control sobre como los recursos son liberados cuando un actor termina-.
    // Los actores se representan fuera y se utilizan llamando referencias. Esto divide el objeto en interno y externo, permitiendo la transparencia
    // para todas las operaciones deseadas: Reiniciar un actor sin actualizar referencias en otro lugar, ubicar el actor objeto en hosts remotos, enviar mensajes 
    // a actores en aplicaciones diferentes. El aspecto mas importante es que no es posible mirar dentro del actor y obtener su comportamiento desde afuera, a menos que el
    // actor publique inprudentemente su estado.
    // 
    
    
    // Patron request-replay. Para cada mensaje enviado, tenemos la opcion de pasar la referencia de 
    // quien env*i*o el mensaje(el remitente -El actorRef-), si enviam
    
    
    // Crear un actor remoto
    // val remote = sistemaDeActores.actorSelection("akka://HelloRemoteSystem@127.0.0.1:5150/user/RemoteActor")
    // http://doc.akka.io/docs/akka/snapshot/general/addressing.html
    // "akka://my-sys/user/service-a/worker1" // purely local
    // "akka.tcp://my-sys@host.example.com:5678/user/service-b" // remote

    // La referencia *Self*, algunas veces el patron de comunicacion es no solo de una via, pero en vez
    // se presta a travez del patron request-replay. De forma explicita lo hacemos anadiendo la referencia 
    // de si mismo como parte del mensaje, asi el receptor puede utilizar la referencia para enviar la 
    // respuesta de regreso al emisor.
    // Este es un escenario comun que es soportado directamente por Akka. Para cada mensaje que ud envia, ud tiene
    // la opcion de pasar la referencia del emisor(El ActorRef). Si enviamos un mensaje desde dentro de un actor,
    // entonces, tenemos acceso a su propia referencia *ActorRef* a travez de su referencia *self*.
    // desde dentro de un actor: greeter.tell(new Greet(), getSelf());
    // En scala accedemos a travez del metodo *self* reference.
    // dentro del actor
    // La referencia "Sender"(remitente)
    // Esta referencia estar� disponible en el actor receptor cuando este procese el mensaje.
    // Cada mensaje esta emparejado con un �nica referencia de emisor, la referencia del emsior "actual" cambiara
    // con cada nuevo mensaje procesado. Si por alguna razon necesitamos utilizar la referencia del emisor mas
    // tarde, tenemos que almacenarla fuera en un campo miembro o similar.
    // en Scala podemos acceder utilizando el m*etodo *sender*. Desde dentro del actor:   //sender ! Greeting(greeting)
    
    // Creamos un buzon de correo de entrada *Inbox*
    
    val buzonCorreo = Inbox.create(sistemaDeActores)
    buzonCorreo.send(actorSpark, "tareaContarEnSpark")
    // Esperamos por 5 segundos la respuesta:
    val tmp1 = buzonCorreo.receive(5.seconds)
    
    println(s"Total registros en actor principal: $tmp1")

}

