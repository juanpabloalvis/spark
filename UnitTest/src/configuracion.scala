import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext

object configuracion {

  def main(args: Array[String]) {

    // doc: https://bigdatatinos.com/2016/04/25/apache-spark-performance-tips/

    val conf = new SparkConf().setAppName("Nombre Aplicacion") // Para pruebas unitarias podemos crear el objeto as*i*new SparkConf(false)
    // Set master, es la URL del cluster, que es Mesos, Yarn o Spark.
    conf.setMaster("local[5]") // se conecta a local para correr con un hilo, y local[4] son 4 cores, o spark://master:7077 para correr en un cluster    
    //conf.setSparkHome("") // La ubicaci*o*n donde spark est*a* instalado en nodos de trabajo.
    // Dynamic context
    //val sc= new SparkContext()  
    /*
     * Para lanzar la aplicaci*o*n: doc http://spark.apache.org/docs/latest/submitting-applications.html
     * /bin/spark-submit \
        --class <main-class>
        --master <master-url> \
        --deploy-mode <deploy-mode> \
        --conf <key>=<value> \
        ... # other options
        <application-jar> \
        [application-arguments]
     * */

    // Un serializador m*a*s r*a*pido que el serialized de java.
    // se debe considerar cuando hay GB de objetos, para grandes cantidades de datos
    // Los objetos serializados en cach*e* utilizan niveles de almacenamiento: 
    // MEMORY_ONLY_SER o MEMORY_AND_DISC_SER. La operaci*o*n de cach*e* se hace un poco lenta, 
    // pero reduce el tiempo del garbage collector.  
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    // Contexto spark
    val sc = new SparkContext(conf)
    // Hay dos maneras de crear RDDs:
    //  1) Paralelizar una colecci*o*n en el driver del programa
    //  2) Referenciando un dataset en un sistema de almacenamiento externo como: un sistema compartido de archivos, HDFS, HBase y cualquier dato ofrecido por Hadoop InputFormat
    // Paralelizaci*o*n 
    val data = Array(1, 2, 3, 4, 5)
    val distData = sc.parallelize(data)
    // val distData = sc.parallelize(data, 10) setear en 10 particiones

    // El nivel de paralelismo: Se refiere al n*u*mero de particiones en las que se divide un RDD.
    // Spark tiene diferentes m*e*todos para determinar el nivel de paralelismo.
    // Si los datos no se dividen bien, no se distribuira bien a travez del cluster. Habr*a* recursos 
    // computacionales ociosos.
    // Spark recomienda 2-3 tareas por Core de CPU.
    // Dos maneras de tunear el paralelismo 
    // 1)utilizando "numPartitions": 
    //val rddreduced = rddoriginal.reduceByKey(_+_, numPartitions = 5)
    // 2) Redistribuir un RDD
    // -repartition(), m*a*s eficiente debido a que no requiere resolver(shuffle)
    // -coalesce(), utilizado cuando el tamaño RDD se reduce y queremos reducir el numero de particiones

    // C:\EspacioTrabajo\ScalaEspacioTrabajo\UnitTest\airline-twitter-sentiment\Tweets.csv
    // Explicaci*o*n del siguiente ejemplo: http://www.tutorialspoint.com/apache_spark/apache_spark_core_programming.htm
    val path = "C:\\EspacioTrabajo\\ScalaEspacioTrabajo\\UnitTest\\airline-twitter-sentiment\\Tweets.csv";
    //  Spark puede leer desde una URI ya sea en la ruta local, o en  hdfs://, o en s3n://,
    //  se pueden importar de acuerdo al patron: textFile("/my/directory"), textFile("/my/directory/*.txt"), y textFile("/my/directory/*.gz")
    val input = sc.textFile(path, 4)
    println("Lineas archivo inicial: ", input.count())
    val filteredInput = input.filter { _.contains("America") } //Equivale a la siguiente linea
    //val filteredInput = input.filter { line => line.contains("America") }
    //(Lineas archivo filtrado: ,3595)
    // ***********************RDD********************
    // RDD soportan dos tipos de operaciones Transformaciones y Acciones:
    // 01) Transformaciones: Crea un dataset apartir de otro existente.
    // 02) Acciones:Retornan un valor del driver del programa despu*e*s de ejecutar un calculo en el dataset.
    // Por ejemplo, "map" es una *transformaci*o*n* que pasa cada elemento del dataset a travez de una funcion y 
    // "reduce" es una *acci*o*n* que agrega todos los elementos del RDD utilizando alguna funci*o*n y retorna el
    // resultado final al driver del programa(aunque tambien hay un "reduceByKey" que retorna un dataset distribuido)

    // Todas las transformaci*o*n en Spark son "lazy", es decir que no se calculan de inmediato,
    // solo son calculadas cuando una *acci*o*n* requiere que un resultado sea retornado al driver del programa
    // Por defecto cada RDD transformado podr*i*a ser calculado cada vez que se ejecuta una acci*o*n sobre este.
    // sin embargo, podr*i*amos persistir un RDD en memoria utilizando el m*e*todo "persist" (o "cache"), en ese caso
    // Spark mantendr*a* los elementos al rededor del cluster para acceso m*a*s r*a*pido la proxima vez que los consultemos. 
    // 
    // Se env*i*a este objeto al cach*e*
    filteredInput.cache();
    println("Lineas archivo filtrado, desde el cache: ", filteredInput.count())
    filteredInput.coalesce(5, shuffle = true)
    println("Cuenta Particiones: ", filteredInput.getNumPartitions)
    println("Primera linea: ", filteredInput.first())

    val lineas = sc.textFile(path) // Aqu*i* se define un RDD, y solo es un apuntador al archivo, no est*a* en memoria, porque es lazy
    val lineLengths = lineas.map(s => s.length) // Este es el resultado de una transofrmaci*o*n, esto no ha sido calculapo porque tambien es lazy
    val totalLength = lineLengths.reduce((a, b) => a + b) // Cuando llamamos el m*e*todo "reduce", es una *acci*o*n* 
    //En este punto Spark divide las tareas de computaci*o*n para ejecutar en m*a*quinas separadas y c*a*da m*a*quina hace su parte local de mapeo y reducci*o*n, 
    // retornando la respuesa al driver del programa.
    // si quisieramos volver a utilizar "lineLengths" posteriormete, podemos hacer lo siguiente:
    lineLengths.persist()
    // antes de reducir, lo que hace que sea guardada en memoria despu*e*s de la primera vez que es calculado.

    //************* Pasando funciones a Sparak*********
    //hay dos maneras de hacer esto:
    // Funci*o*n an*o*nima utilizada para piezas de c*o*digo cortas y m*e*todos est*a*ticos en objeto singleton
    // investigar...
    lineLengths.map(MyFunctions.func1)

    //**************** Closures (cierres) ******
    // una de las cosas m*a*s dificiles de Spark es entender el alcance y ciclo de vida de las variables y m*e*todos cuando
    // ejecutamos codigo a travez de un cluster. Operaciones RDD que modifican variables fuera de su alcance pueden ser fuente de confusi*o*n
    // Consideremos la suma del siguiente elemento, el cual se podr*i*a comportar diferente dependiendo sobre la ejecuci*o*n que este ocurriendo
    // sobre la misma JVM. Un ejemplo es cuando ejecutamos Spark en modo local vs un Cluster. En este ejemplo miraremos un codigo que 
    // hace foreach() para incrementar un contador.
    // 
    var counter = 0
    var rdd = sc.parallelize(data)
    // Wrong: Don't do this!!
    rdd.foreach(x => counter += x)
    println("Counter value: " + counter)

    // Al ejecutar Jobs, Spark separa el procesamiento de las operaciones RDD en tareas, 
    // cada una de las cuales es ejecutada por un ejecutor. Antes de la ejecuci*o*n, Spark
    // calcula la tarea de *closure*. El *closure* son esas variables y m*e*todos los cuales deben ser visibles para el ejecutor que 
    // desarrolla sus calculos sobre el RDD(en este caso foreach()). Este closure es serializado y enviado a cada ejecutor.
    
    // las variables dentro del closure enviadas a cada ejecutor son ahora copias y as*i* susecivamente. cuando un 
    // "contador" es referenciado dentro de la funci*o*n foreach(), ya no est*a* el "contador" en el nodo controlador.
    // existe a*u*n un "contador" en la memoria del driver del nodo controlador, pero no es visible a los nodos ejecutores.
    // los ejecutores solo ven una copia del closure serializado. As*i* el valor final del "contador" ser*a* cero para 
    // todas las operaciones sobre el "contador" que fue referenciado el valor dentro del closure serializado.    
    // En modo local, como se ejecuta en la misma JVM, el driver referenciar*a* al mismo "contador" original, y podr*i*a actualizarlo correctamente.
    // Para asegurar un comportamiento bien definido en uno de esto escenarios, deber*i*amos usar un *Acumulator*. 
    // Son utilizados espec*i*ficamente para brindar un mecainsmo para asegurar la actualizaci*o*n de una variable,
    // cuando la ejecuci*o*n est*a* dividida en varios nodos de trabajo en un cluster.
    // En general, Spark no define o garantiza el comportamiento de las mutaciones de objetos referenciados fuera de los Closures.
    // se debe utilizar un Accumulator envez de agregados globales.
    
    // **************** Imprimiendo elemntos de un RDD.*************
    // Un idioma com*u*n es imprimir elementos RDDs utlizando rdd.foreach(println) o rdd.map(println)
    // en una sola m*a*quina esto generar*a* la salida esperada, e imprimir*a* todos los elementos RDDs. Sin embargo en modo cluster,
    // la salida a la salida standar, siendo llamada por los ejecutores est*a* ahora escribiendo a la salida est*a*ndar del ejecutor,  
    // no sobre el driver, entonces la salida est*a*ndar sobre el driver no mostrar*a* esto. Para imprimir todos los elementos en el driver, 
    // podemos utilizar el m*e*todo "collect()" para primero traer el RDD al nodo controlador(driver) as*i*: rdd.collect().foreach(println)
    //filteredInput.collect().foreach(println)
    // esto puede causar que el driver se quede sin memoria, porque collect() recupera el RDD completo en una sola m*a*quina, si necesitamos 
    // mostrar pocos elementos del RDD, un enfoque m*a*s seguro es utilizar rdd.take(100).foreach(println)
    filteredInput.take(100).foreach(println)
    
    // *************** Trabajando con parejas clave-valor *************
    // Mientras que la mayor*i*a de operaciones se trabajan sobre RDD que contienen cualquier tipo de objetos, 
    // pocas operaciones son *u*nicamente disponibles sobre RDDs de parejas clave-valor.
    // En scala, esas operaciones son *u*nicamente disponibles sobre RDDs que contienen objetos "Tuple2"(construidos en lenguaje de tuplas)
    // Por ejemplo, el siguiente c*o*digo utiliza la operaci*o*n de reducci*o*n "reducedByKey" sobre las parejas clave-valor para contar cuantas
    // veces cada linea de texto aparece en un archivo:
    
    val pairs = filteredInput.map(s => (s, 1))
    val counts = pairs.reduceByKey((a, b) => a + b)
    // ahora podr*i*amos utilizar counts.sortByKey(), por ejemplo, para ordenar parejas alfabeticamente y finalmente counts.collect() para traer
    // de regreso al programa controlador el array de objetos
    counts.sortByKey()
    counts.collect()
    // Nota: Cuando utilizamos operaciones con parjeas clave-valor, debemos asegurarnos que el m*e*todo equals() est*e* acompañado de un 
    // m*e*todo hashCode() coincidente.
    
    // *************** TRANSFORMACIONES  *************
    // *************** ACCIONES  *************
    // http://spark.apache.org/docs/latest/programming-guide.html#shuffle-operations
    //Operaciones de barajar
    
    
    //Spark brinda una manera conveniente de interactuar con consultas sobre grandes datasets utilizando el motor de Spark
    // utilizando un tipo especial de esquema llamado SchemaRDD. Se puede crear a partir de otros RDDs o 
    // desde formatos externos como archivos "Parquet", datos JSON o ejecutando HQL sobre HIVE.
    // Hay dos tipos de contextos HIVE y SQL 
    /* EJEMPLO
      val sqlContext = new SQLContext(sc)
      val r = sc.textFile("/Users/akuntamukkala/temp/customers.txt") 
      val records = r.map(_.split('|'))
      val c = records.map(r=>Customer(r(0),r(1).trim.toInt,r(2),r(3))) c.registerAsTable("customers")
      
      // Consulta
      sqlContext.sql("select * from customers where gender="M" and age < 30").collect().foreach(println)
     **/

  }

  object MyFunctions {
    def func1(s: Int): Int = {
      var num: Int = 0
      num = s * 2
      return num
    }
  }
}