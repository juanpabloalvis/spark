import scala.concurrent._
import ExecutionContext.Implicits.global

object FutureDemo extends App {
  val x: Future[Int] = Future {
    Thread.sleep(1200)
    // external API call
   // RandomNumberGenService.gen()
    111
  }
  println("Esperando para completar el Futuro")
  x.onComplete {
    case scala.util.Success(e)     => println(e) // returns 323
    case scala.util.Failure(excep) => excep.printStackTrace()
  }
  x.onSuccess {
    case mn => println(mn) // returns 323
  }
  Thread.sleep(2000)
}