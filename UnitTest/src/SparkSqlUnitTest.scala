import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import java.util.Properties

object SparkSqlUnitTest {
  def main(args: Array[String]) {

    { println("") }

    val conf = new SparkConf().setAppName("SparkSqlTest") // Para pruebas unitarias podemos crear el objeto as*i*new SparkConf(false)
    // Set master, es la URL del cluster, que es Mesos, Yarn o Spark.
    conf.setMaster("local[5]") // se conecta a local para correr con un hilo, y local[4] son 4 cores, o spark://master:7077 para correr en un cluster
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    conf.set("mysql.url", "jdbc:mysql://127.0.0.1:3306/sakila")
    conf.set("mysql.urlWrite", "jdbc:mysql://127.0.0.1:3306/spark")
    conf.set("mysql.driver", "com.mysql.jdbc.Driver")
    conf.set("mysql.username", "root")
    conf.set("mysql.password", "root")

    // Contexto spark
    val sc = new SparkContext(conf)

    //***********************************SPARK SQL*************************
    // SparkSQL es un m*o*doluo para procesar datos estructurados. A diferencia del API base de SparkRDD, las interfaces ofrecidas por
    // SparkSQL proporcionan a Spark m*a*s informaci*o*n acerca de la estructura de los datos y el calcluo que est*a* siendo ejecutado. Internamente Spark
    // utiliza informaci*o*n extra para desarrollar optimizaciones adicionales. Hay varias maneras de interactuar con SparkSQL incluyendo SQL, 
    // el API de DataFrames y el API de Datasets. Cuando calculamos un resultado el mismo motor de ejecuci*o*n es utilizdo, 
    // independiente del API/Lenguaje que est*e* utilizando para expresar el calculo. Esta unificaci*o*n significa que los desarolladores
    // pueden facilmente cambiar entre varias APIs, basadas sobre cu*a*l brinda la forma mas natural de expresar una transformaci*o*n dada.
    // Todos los ejemplos utlizan datos de ejemplo incluidos en la distribuci*o*n de Spark y pueden ejecutarse en la shell *spark-shell* o *sparkR*
    //********************SQL********************
    // uno de los usos de Spark SQL es ejecutar consultas SQL escritas o con s*i*ntaxis b*a*sica o Hive SQL. SparkSQL tambi*e*n puede ser utilizado para
    // leer datos de una instalaci*o*n existente de Hive. Cuando ejecutamos SQL dentro de otro lenguaje de programaci*o*n los resultados ser*a*n retornado
    // como un DataFrame. Podemos interactuar con la interface SQL utilizando la linea de comandos o sobre JDBC:http://spark.apache.org/docs/latest/sql-programming-guide.html#running-the-spark-sql-cli  
    //********************DATA FRAMES********************
    // Un DataFrame es una colecci*o*n distribuida de datos organizada en columnas nombradas. Esto conceptualmente equivale a una tabla en una base de datos
    // relacional o un data frame en R/Python, pero con optimizaciones m*a*s ricas por debajo. DataFrames pueden ser construidos desde una amplio array de 
    // fuentes como: Archivos de datos estructurados, tablas en Hive, bases de datos externas o RDDs existentes.
    // El API DataFrame est*a* disponible en Scala, Python y R
    //********************DATASETs********************
    // Es una nueva interface experimental que intenta brindar beneficios de RDDs(fuerte tipado, y la habilidad de las poderosas funciones lambda)
    // con los beneficios del motor de ejecuci*o*n optimizado SparkSQL. Un dataset puede ser construido desde objetos de la JVM y luego manipularlos 
    // utilizando transformaciones funcionales(map, flatMap, filter, etc)
    // El API del Dataset unificado puede ser utilizado por Scala y por Java. 
    // Punto de inicio:
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    // Este import se utiliza para convertir un RDD en un DataFrame
    import sqlContext.implicits._
    // En adici*o*n al contexto b*a*sico, tambien se puede crear un contexto HiveContext, el cual brinda un super conjunto de funcionalidades que
    // ofrece el conjunto b*a*sico SQLContext b*a*sico. Caracter*i*sticas adicionales incluyen la habilidad de escribir consultas utilizando un parseador, 
    // HiveQL, acceso a Hive UDF y la habilidad de leer datos de tablas Hive. ... http://spark.apache.org/docs/latest/sql-programming-guide.html#starting-point-sqlcontext

    // La variante espc*i*fica de SQL que es utilizada para parsear las consultas tambi*e*n puede ser seleccionada utilizando la opci*o*n *spark.sql.dialect*
    // Este par*a*metro puede ser cambiado utilizando el m*e*todo *setConf* o *SQLContext* o por utilizar el comando en SQL "SET key=value". 
    // Para un SQLContext, el *u*nico dialecto disponible es "sql" el cu*a*l utiliza un simple parser SQL ofrecido por SparkSQL. para hive...

    // ****************Creando DataFrames
    // Con SQLContext las aplicaciones pueden crear DataFrameS desde un RDD existente, desde una tabla Hive o desde dataSource, 
    // Un ejemplo basado en el contenido de un archivo JSON
    //muestra el contenido del dataframe
    //val df = sqlContext.read.json("..\\UnitTest\\archivos\\rows3.json")
    val df = sqlContext.read.json("C:\\spark-1.6.1\\examples\\src\\main\\resources\\people.json")
    // muestra el contenido del dataframe
    df.show()

    // Imprime el esquema en formato de arbol
    df.printSchema();

    //registramos este esquema como una tabla para utiilzar posteriormente 
    df.registerTempTable("damierd")

    // Seleccionar la columna name
    df.select("name").show

    // Seleccionar todo, incrementando la columna *age* en 1
    df.select(df("name"), df("age") + 1).show

    // seleccione las personas mayores a 12
    df.select(df("age") > 21).show

    // contar personas por edad, agrupando
    df.groupBy("age").count().show

    //val sqlContext = ... // An existing SQLContext
    val damierd = sqlContext.sql("SELECT * FROM damierd")

    val url = "jdbc:mysql://127.0.0.1:3306/sakila"
    val urlWrite = "jdbc:mysql://127.0.0.1:3306/spark"
    val driver = "com.mysql.jdbc.Driver"
    val username = "root"
    val password = "root"
    val sqlTableName = "(select c.city, p.country from city c left join country p on c.country_id = p.country_id)as ciudades"
    val prop = new Properties()
    prop.put("user", username)
    prop.put("password", password)

    // ejecutar consultas mediante programacion:
  //  val sqlContextFirstExample = new org.apache.spark.sql.SQLContext(sc)
    val dfFirtsExample = sqlContext.read.format("jdbc") //DATA FRAME
      .option("url", url)
      .option("driver", driver)
      .option("dbtable", sqlTableName)
      .option("user", username)
      .option("password", password)
      .load()
    
    dfFirtsExample.registerTempTable("sakilacities")
      
    val cities = sqlContext.sql("SELECT city FROM sakilacities")
    println("cuenta sakilacities",cities.count())
    val countries = sqlContext.sql("SELECT country FROM sakilacities group by country")
    println("cuenta sakilacountries",countries.count())
    
    // dfFirtsExample.select("select c.city, p.country from city c left join country p on c.country_id = p.country_id")
    //dfFirtsExample.select("city, country")
    //*********************Joins *********************
    // http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.DataFrame
    //cities.join(countries)
    
    // Los Datasets son similares a los RDDs, sin embargo en vez de utilizar serializador de Java o Kyro,
    // estos utilizan *Encoder* para serializar objetos para procesar o tranmitir sobre la red.
    // Los encoders son codigo generado dinamicamente y utilizan el formato que permite a Spark realizar muchas operaciones
    // como filtrar, ordenar, realizar hash si deserializar los bytes de regreso al objeto.

  }

}