import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import java.util.Properties
object SparkUnitTest {

  def main(args: Array[String]) {

    { println("") }

    // doc: https://bigdatatinos.com/2016/04/25/apache-spark-performance-tips/

    val conf = new SparkConf().setAppName("Nombre Aplicacion") // Para pruebas unitarias podemos crear el objeto as*i*new SparkConf(false)
    // Set master, es la URL del cluster, que es Mesos, Yarn o Spark.
    conf.setMaster("local[5]") // se conecta a local para correr con un hilo, y local[4] son 4 cores, o spark://master:7077 para correr en un cluster
    
    //conf.setSparkHome("") // La ubicaci*o*n donde spark est*a* instalado en nodos de trabajo.
    // Dynamic context
    //val sc= new SparkContext()  
    /*
     * Para lanzar la aplicaci*o*n: doc http://spark.apache.org/docs/latest/submitting-applications.html
     * /bin/spark-submit \
        --class <main-class>
        --master <master-url> \
        --deploy-mode <deploy-mode> \
        --conf <key>=<value> \
        ... # other options
        <application-jar> \
        [application-arguments]
        
		Ejemplo: 
					C:\spark-1.6.1\bin>spark-submit --jars C:\Users\Juan_Alvis\Documents\NetBeansProjects\lib\mysql-connector-java-5.1.36\mysql-connector-java-5.1.36-bin.jar,C:\activator-dist-1.3.10\repository\com.typesafe.akka\akka-actor_2.10\2.3.11\jars  --class SparkUnitTest --master local[5] C:\EspacioTrabajo\ScalaEspacioTrabajoMars\Test.jar
        
     * */

    // Un serializador m*a*s r*a*pido que el serialized de java.
    // se debe considerar cuando hay GB de objetos, para grandes cantidades de datos
    // Los objetos serializados en cach*e* utilizan niveles de almacenamiento: 
    // MEMORY_ONLY_SER o MEMORY_AND_DISC_SER. La operaci*o*n de cach*e* se hace un poco lenta, 
    // pero reduce el tiempo del garbage collector.  
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    // Contexto spark
    val sc = new SparkContext(conf)
    // Hay dos maneras de crear RDDs:
    //  1) Paralelizar una colecci*o*n en el driver del programa
    //  2) Referenciando un dataset en un sistema de almacenamiento externo como: un sistema compartido de archivos, HDFS, HBase y cualquier dato ofrecido por Hadoop InputFormat
    // Paralelizaci*o*n 
    val data = Array(1, 2, 3, 4, 5)
    val distData = sc.parallelize(data)
    // val distData = sc.parallelize(data, 10) setear en 10 particiones

    // El nivel de paralelismo: Se refiere al n*u*mero de particiones en las que se divide un RDD.
    // Spark tiene diferentes m*e*todos para determinar el nivel de paralelismo.
    // Si los datos no se dividen bien, no se distribuira bien a travez del cluster. Habr*a* recursos 
    // computacionales ociosos.
    // Spark recomienda 2-3 tareas por Core de CPU.
    // Dos maneras de tunear el paralelismo 
    // 1)utilizando "numPartitions": 
    // val rddreduced = rddoriginal.reduceByKey(_+_, numPartitions = 5)
    // 2) Redistribuir un RDD
    // -repartition(), m*a*s eficiente debido a que no requiere resolver(shuffle)
    // -coalesce(), utilizado cuando el tama*n*o RDD se reduce y queremos reducir el numero de particiones

    // C:\EspacioTrabajo\ScalaEspacioTrabajo\UnitTest\airline-twitter-sentiment\Tweets.csv
    // Explicaci*o*n del siguiente ejemplo: http://www.tutorialspoint.com/apache_spark/apache_spark_core_programming.htm
    // val path = "..\\UnitTest\\airline-twitter-sentiment\\Tweets.csv";
    val path = "C:\\EspacioTrabajo\\ScalaEspacioTrabajoMars\\Spark\\UnitTest\\airline-twitter-sentiment\\Tweets.csv";
    //  Spark puede leer desde una URI ya sea en la ruta local, o en  hdfs://, o en s3n://,
    //  se pueden importar de acuerdo al patron: textFile("/my/directory"), textFile("/my/directory/*.txt"), y textFile("/my/directory/*.gz")

    val input = sc.textFile(path, 4)
    println("Lineas archivo inicial: ", input.count())
    val filteredInput = input.filter { _.contains("America") } //Equivale a la siguiente linea
    //val filteredInput = input.filter { line => line.contains("America") }
    //(Lineas archivo filtrado: ,3595)
    // ***********************RDD********************
    // RDD soportan dos tipos de operaciones Transformaciones y Acciones:
    // 01) Transformaciones: Crea un dataset apartir de otro existente.
    // 02) Acciones: Retornan un valor del controlador del programa despu*e*s de ejecutar un calculo en el dataset.
    // Por ejemplo, "map" es una *transformaci*o*n* que pasa cada elemento del dataset a travez de una funcion y 
    // "reduce" es una *acci*o*n* que agrega todos los elementos del RDD utilizando alguna funci*o*n y retorna el
    // resultado final al driver del programa(aunque tambien hay un "reduceByKey" que retorna un dataset distribuido)

    // Todas las transformacione en Spark son "lazy", es decir que no se calculan de inmediato,
    // solo son calculadas cuando una *acci*o*n* requiere que un resultado sea retornado al driver del programa
    // Por defecto cada RDD transformado podr*i*a ser calculado cada vez que se ejecuta una acci*o*n sobre este.
    // sin embargo, podr*i*amos persistir un RDD en memoria utilizando el m*e*todo "persist" (o "cache"), en ese caso
    // Spark mantendr*a* los elementos al rededor del cluster para acceso m*a*s r*a*pido la proxima vez que los consultemos. 
    // 
    // Se env*i*a este objeto al cach*e*
    filteredInput.cache();
    println("Lineas archivo filtrado, desde el cache: ", filteredInput.count())
    // Validar uso de coalesce
    filteredInput.coalesce(5, shuffle = true)
    println("Cuenta Particiones: ", filteredInput.getNumPartitions)
    println("Primera linea: ", filteredInput.first())

    val lineas = sc.textFile(path) // Aqu*i* se define un RDD, y solo es un apuntador al archivo, no est*a* en memoria, porque es lazy
    val lineLengths = lineas.map(s => s.length) // Este es el resultado de una transofrmaci*o*n, esto no ha sido calculado porque tambien es lazy
    val totalLength = lineLengths.reduce((a, b) => a + b) // Cuando llamamos el m*e*todo "reduce", es una *acci*o*n* 
    //En este punto Spark divide las tareas de computaci*o*n para ejecutar en m*a*quinas separadas y c*a*da m*a*quina hace su parte local de mapeo y reducci*o*n, 
    // retornando la respuesa al driver del programa.
    // si quisieramos volver a utilizar "lineLengths" posteriormete, podemos hacer lo siguiente:
    lineLengths.persist()
    // antes de reducir, lo que hace que sea guardada en memoria despu*e*s de la primera vez que es calculado.

    //************* Pasando funciones a Sparak*********
    //hay dos maneras de hacer esto:
    // Funci*o*n an*o*nima utilizada para piezas de c*o*digo cortas y m*e*todos est*a*ticos en objeto singleton
    // investigar...
    lineLengths.map(MyFunctions.func1)

    //**************** Closures (cierres) ******
    // una de las cosas m*a*s dificiles de Spark es entender el alcance y ciclo de vida de las variables y m*e*todos cuando
    // ejecutamos codigo a travez de un cluster. Operaciones RDD que modifican variables fuera de su alcance pueden ser fuente de confusi*o*n
    // Consideremos la suma del siguiente elemento, el cual se podr*i*a comportar diferente dependiendo sobre la ejecuci*o*n que este ocurriendo
    // sobre la misma JVM. Un ejemplo es cuando ejecutamos Spark en modo local vs un Cluster. En este ejemplo miraremos un codigo que 
    // hace foreach() para incrementar un contador.
    // 
    var counter = 0
    var rdd = sc.parallelize(data)
    // Wrong: Don't do this!!
    rdd.foreach(x => counter += x)
    println("Counter value: " + counter)

    // Al ejecutar Jobs, Spark separa el procesamiento de las operaciones RDD en tareas, 
    // cada una de las cuales es ejecutada por un ejecutor. Antes de la ejecuci*o*n, Spark
    // calcula la tarea de *closure*. El *closure* son esas variables y m*e*todos los cuales deben ser visibles para el ejecutor que 
    // desarrolla sus calculos sobre el RDD(en este caso foreach()). Este closure es serializado y enviado a cada ejecutor.

    // las variables dentro del closure enviadas a cada ejecutor son ahora copias y as*i* susecivamente. Cuando un 
    // "contador" es referenciado dentro de la funci*o*n foreach(), ya no est*a* el "contador" en el nodo controlador.
    // existe a*u*n un "contador" en la memoria del driver del nodo controlador, pero no es visible a los nodos ejecutores.
    // los ejecutores solo ven una copia del closure serializado. As*i* el valor final del "contador" ser*a* cero para 
    // todas las operaciones sobre el "contador" que fue referenciado el valor dentro del closure serializado.    
    // En modo local, como se ejecuta en la misma JVM, el driver referenciar*a* al mismo "contador" original, y podr*i*a actualizarlo correctamente.
    // Para asegurar un comportamiento bien definido en uno de esto escenarios, deber*i*amos usar un *Acumulator*. 
    // Son utilizados espec*i*ficamente para brindar un mecainsmo para asegurar la actualizaci*o*n de una variable,
    // cuando la ejecuci*o*n est*a* dividida en varios nodos de trabajo en un cluster.
    // En general, Spark no define o garantiza el comportamiento de las mutaciones de objetos referenciados fuera de los Closures.
    // se debe utilizar un Accumulator envez de agregados globales.

    // **************** Imprimiendo elemntos de un RDD.*************
    // Un idioma com*u*n es imprimir elementos RDDs utlizando rdd.foreach(println) o rdd.map(println)
    // en una sola m*a*quina esto generar*a* la salida esperada, e imprimir*a* todos los elementos RDDs. Sin embargo en modo cluster,
    // la salida a la salida estandar, siendo llamada por los ejecutores est*a* ahora escribiendo a la salida est*a*ndar del ejecutor,  
    // no sobre el driver, entonces la salida est*a*ndar sobre el driver no mostrar*a* esto. Para imprimir todos los elementos en el driver, 
    // podemos utilizar el m*e*todo "collect()" para primero traer el RDD al nodo controlador(driver) as*i*: rdd.collect().foreach(println)
    //filteredInput.collect().foreach(println)
    // esto puede causar que el driver se quede sin memoria, porque collect() recupera el RDD completo en una sola m*a*quina, si necesitamos 
    // mostrar pocos elementos del RDD, un enfoque m*a*s seguro es utilizar rdd.take(100).foreach(println)
    filteredInput.take(100).foreach(println)

    // *************** Trabajando con parejas clave-valor *************
    // Mientras que la mayor*i*a de operaciones se trabajan sobre RDD que contienen cualquier tipo de objetos, 
    // pocas operaciones son *u*nicamente disponibles sobre RDDs de parejas clave-valor.
    // En scala, esas operaciones son *u*nicamente disponibles sobre RDDs que contienen objetos "Tuple2"(construidos en lenguaje de tuplas)
    // Por ejemplo, el siguiente c*o*digo utiliza la operaci*o*n de reducci*o*n "reduceByKey" sobre las parejas clave-valor para contar cuantas
    // veces cada linea de texto aparece en un archivo:
    val pairs = filteredInput.map(s => (s, 1))

    val counts = pairs.reduceByKey((a, b) => a + b)
    val countss = pairs.reduceByKey((a, b) => a)

    // ahora podr*i*amos utilizar counts.sortByKey(), por ejemplo, para ordenar parejas alfabeticamente y finalmente counts.collect() para traer
    // de regreso al programa controlador el array de objetos
    counts.sortByKey()
    countss.sortByKey()
    counts.collect()
    countss.collect()
    println(counts.count(), "-", countss.count())
    counts.take(10).foreach(println)
    countss.take(10).foreach(println)
    // Nota: Cuando utilizamos operaciones con parjeas clave-valor, debemos asegurarnos que el m*e*todo equals() est*e* acompa*n*ado de un 
    // m*e*todo hashCode() coincidente.

    // *************** TRANSFORMACIONES  *************
    // http://spark.apache.org/docs/latest/programming-guide.html#transformations
    // *************** ACCIONES  *************
    // http://spark.apache.org/docs/latest/programming-guide.html#actions
    // http://spark.apache.org/docs/latest/programming-guide.html#shuffle-operations

    //*************** OPERACIONES SHUFFLE  ************* (SHUFFLE= Baraja, aletoriedad)
    // Es un mecanismo para redistribuir datos, este est*a* agrupado en diferentes particiones. T*i*picamente envuelve copiar datos 
    // a travez de ejecutores y m*a*quinas, haciendo la operaci*o*n shuffle costosa y compleja.
    // Durante los calculos, una tarea simple operar*a* sobre una porci*o*n, as*i* para organizar los datos para ejecutar 
    // una tarea simple de reducir "reduceByKey", Spark necesita realizar una operaci*o*n todo-a-todo. Este debe leer desde
    // todas las particiones para hallar todos los valores para todas las claves, y luego traer todos los valores a travez de las 
    // particiones para calcular el resultado final para cada llave, esto es llamado "Shuffle"(barajar).
    // Aunque el conjunto de nuevos elementos en cada partici*o*n de nuevos datos "Shuffled" ser*a* determinista(Ej, con la misma entrada produce la misma salida, no determinista: con la misma entrada produce diferente salida.),
    // y tambi*e*n lo es el *o*rden de las particiones mismas, el *o*rden de esos elementos no lo es. Si alguien desea ordenar datos siguiendo la (baraja)
    // es posible utilizar:
    // * "maxPartitions" para ordenar cada partici*o*n utilizando por ejemplo, .sorted
    // * "repartitionAndSortWithinPartitions" para ordenar particiones eficientemente mientras reparticionamos simultaneamente.
    // * "sortBy" para hacer un RDD ordenado Globalmente.
    // Operaciones que pueden causar un aleatoridad incluyendo operaciones de reparticion como "repartition" y 
    // "coalesce"(juntarse), operaciones "ByKey" (excepto contar) como "groupByKey" y "reduceByKey, y operaciones JOIN como "cogruup"  y "join"

    //*************** IMPACTO DE RENDIMIENTO ************* 
    // Shuffe es una operaci*o*n costosa debido a que envuelve I/O en disco, serializaci*o*n de datos, y I/O de red. 
    // Para organizar datos para el shuffle, Spark genera un conjunto de tareas, tareas de *mapeo* para organizar los datos, y un conjunto
    // de tareas de *reduccion* para agregar.
    // Esta nomenclatura viene de MapReduce y no esta relacionado directamente con las operaciones de "mapeo" y "reudci*o*n" de Spark.
    // Internamente, los resultados de tareas de mapeo individual son mantenidas en memoria hasta que no puedan llenarse. Entonces,
    // estos son ordenados basados en la partici*o*n destino y escritos en un simple archivo. En el lado de la reducci*o*n, 
    // las tareas leen los bloques ordenados relevantes.

    //*************** PERSISTENCIA RDD *************
    // una de las capacidades m*a*s importantes en Spark es la persistir(o caching) un dataset en memoria a travez de operaciones.
    // Cuando persistimos un RDD cada nodo almacena cualquier particion de este que se calcula en memoria y se reutilizan en otras acciones 
    // sobre el dataset(o sobre otros datasets derivados de este).
    // Esto permite que acciones futuras sean mucho m*a*s r*a*pidas(frecuentemente m*a*s de 10X). Caching es una herramienta clave para 
    // algor*i*tmos iterativos y uso interactivo r*a*pido.
    // Un RDD se puede marcar para persistir utilizando los m*e*todos "persist()" o "cache()" sobre el RDD. La primera vez que se calcular
    // en una "acci*o*n", este se mantendr*a* en los nodos de memoria. El cach*e* de spark es fault-tolerant, si cualquier partici*o*n del RDD es 
    // perdida, este autom*a*ticamente ser*a* recalculada utilizando las transformaciones que originalmente se crearon.
    // En adici*o*n, cada RDD persistido puede ser almacenado utilizando diferentes niveles de almacenamiento, permitiendo por ejemplo,
    // persisitir el dataset en disco, persistir en memoria pero como objetos java serializados(para ahorrar espacio), replicaci*o*n a travez de nodos, 
    // o almacenar fuera de la pila en *Tachyon*, estos niveles son configurados pasando un objeto "StorageLevel" (Scala, Java, Python) a persist(). 
    // El m*e*todo cache() es un atajo para utilizar el nivel de almacenamiento por defecto, el cual es *StorageLevel.MEMORY_ONLY*, (almacenar 
    // objetos deserializados en memoria), la descripci*o*n completa de los niveles de almacenamiento:http://spark.apache.org/docs/latest/programming-guide.html#rdd-persistence 
    // Para bajar un objeto del cach*e* podemos utilizar el m*e*todo RDD.unpersist()

    //*************** VARIABLES COMPARTIDAS *************
    // Normalmente cuando una funci*o*n pasada a una operaci*o*n Spark(*map* or *reduce*), esta es ejecutada en un nodo cluster remoto,
    // este funciona sobre copias separadas de todas las variables utilizadas en la funcion. Estas variables son copiadas en cada m*a*quina, y no hay 
    // actualizaci*o*n sobre las m*a*quinas remotas son propagadas de regreso al programa controlador. Soporte General, 
    // variables compartidas de lectura y escritura a travez de tareas podr*i*a ser ineficiente. Sin embargo, Spark provee dos tipos limitados de 
    // variables compartidas, para dos patrones de uso com*u*n: variables Broadcast y acumuladores.
    // Variables broadcast: permiten tener en cach*e* una variable de lectura *u*nicamente en cada m*a*quina, en vez de enviar una copia de este con tareas.
    // Luego puede ser utilizada, por ejemplo, para darle a cada nodo una copia de un gran dataset de una forma eficiente. Spark tambi*e*n intenta distribuir variables
    // de broadcast utilizando algoritmos de broadcast para reducir el costo de comunicaci*o*n.
    // Las "Acciones" Spark son ejecutadas a travez de un conjunto de etapas, separadas por operaciones "shuffle" distribuidas. Spark autom*a*ticamente transmite los datos comunes
    // necesarios por las tareas dentro de cada etapa. La transmisi*o*n de datos de esta manera es cacheado en una forma serializada y deserializado antes de ejecutar cada tarea.
    // Esto significa que crear variables broadcas explicitamente es *u*nicamente *u*til cuando tareas a travez de multiples etapas necesitan los mismos datos 
    // o cuando el cacheo de datos en forma deserializada es importante.

    // Las variables de broadcast son creados en forma de una variable *v*, llamando el m*e*todo "SparkContext.broadcast(v)". La variable 
    // broadcast es una envoltura al rededor de *v* y su valor puede ser accedido llamando el m*e*todo *value*. Ejemplo:
    val v = Array(1, 2, 3)
    val broadcastVar = sc.broadcast(v)
    println(broadcastVar.value)
    // Despu*e*s de creada, la variable a utilizar debe ser "broadcastVar" y no "v", porque v no viaja a los nodos m*a*s que una vez. El objeto *v* no
    // deber*i*a ser modificado despu*e*s de la transmisi*o*n, con el fin que todos los nodos obtengan el mismo valor de la variable broadcast.

    //*************** ACUMULADORES *************
    // acumuladores son variables que *u*nicamente "suman" a travez de una operaci*o*n asociativa y por lo tanto pueden 
    // ser soportadas de manera eficiente en paralelo. Puden ser utilizadas para implementar contadores( como en MapReduce) o sumas. Spark soporta 
    // acumuladores de tipo numericos, los progamadores pueden a*n*adir soporte a nuevos tipos. Si los acumuladores son creado con un nombre, se mostraran en Spart UI.
    // El acumulador est*a* creado desde un valor inicial *v* llamando el m*e*todo "SparkContext.accumulator(v)". Las tareas que se ejecutan sobre el cluster 
    // pueden ser adicionadas a este utlizando el m*e*todo "add" o el operador +=. Sin embargo, no pueden leer su valor. Unicamente el programa 
    // controlador puede leer el valor del acumulador, utilizando el m*e*todo *value*. Ejemplo:
    val accum = sc.accumulator(0, "My Accumulator")
    sc.parallelize(Array(1, 2, 3, 4)).foreach(x => accum += x)
    accum.value
    // Si requerimos crear acumuladores, validar el ejemplo que est*a* para n*u*meros enteros.http://spark.apache.org/docs/latest/programming-guide.html#accumulators

    //*************** DESPLEGANDO EN UN CLUSTER *************
    // http://spark.apache.org/docs/latest/submitting-applications.html
    // archivo properties: http://stackoverflow.com/questions/31115881/how-to-load-java-properties-file-and-use-in-spark?answertab=votes#tab-top

    // Conexi*o*n MYSQL
    val sqlContextFirstExample = new org.apache.spark.sql.SQLContext(sc)

    val url = "jdbc:mysql://127.0.0.1:3306/sakila"
    val urlWrite = "jdbc:mysql://127.0.0.1:3306/spark"
    val driver = "com.mysql.jdbc.Driver"
    val username = "root"
    val password = "root"
    val sqlTableName = "customer"
    val prop = new Properties()
    prop.put("user", username)
    prop.put("password", password)

    val dfFirtsExample = sqlContextFirstExample.read.format("jdbc") //DATA FRAME
      .option("url", url)
      .option("driver", driver)
      .option("dbtable", sqlTableName)
      .option("user", username)
      .option("password", password)
      .load()

    // Imprimir el esquema de la tabla
    dfFirtsExample.printSchema()

    // dataframe_mysql.registerTempTable("names")     //Register the data as a temp table for future SQL queries
    //df.sqlContext.sql("select * from names").collect.foreach(println) //We are now in position to run some SQL such as

    //val countResult = sqlContext.sql("SELECT COUNT(*) FROM film_text").collect()
    val countsByMarket = dfFirtsExample.groupBy("first_name").count()
    countsByMarket.show()
    countsByMarket.persist()

    // RDD TO DataFrame para escribir en BD
    //val newDf = dist.toDF()
    // newDf.write.jdbc(…)
    countsByMarket.write.mode(SaveMode.Append).jdbc(urlWrite, "TablaNueva", prop)
    //countsByMarket.insertIntoJDBC(url, sqlTableName, false)

    // Guardamos el resultado en formta Parquet
    //countsByMarket.write.parquet("archivo.parquet")

    /*
    //Escribir un RDD a base de datos
    countsByMarket.foreachPartition {
      it =>
        val conn = DriverManager.getConnection(url, username, password)
        val del = conn.prepareStatement("INSERT INTO tweets (ID,Text) VALUES (?,?) ")
        for (bookTitle <- it) {
          del.setString(1, bookTitle.toString)
          del.setString(2, "my input")
          del.executeUpdate
        }
    }
    */

    
    
    
    
    //println("-->01")
    // imprime el esquema en formato de *a*rbol
    //df.first()
    //df.select("meta.view.name").show()
    //println("-->02")
    //val df2 = df.select()
    //df.select("meta").select("view").select("name").show()
    
    
    
    
    /*Ejemplo ROW */
    val row = Row(1, true, "a string", null)
    // row: Row = [1,true,a string,null]
    val firstValue = row(0)
    // firstValue: Any = 1
    val fourthValue = row(3)
    

    // Hay dos tipos de contextos HIVE y SQL 
    /* EJEMPLO
      val sqlContext = new SQLContext(sc)
      val r = sc.textFile("/Users/akuntamukkala/temp/customers.txt") 
      val records = r.map(_.split('|'))
      val c = records.map(r=>Customer(r(0),r(1).trim.toInt,r(2),r(3))) c.registerAsTable("customers")
      
      // Consulta
      sqlContext.sql("select * from customers where gender="M" and age < 30").collect().foreach(println)
     **/

    // z();
    
    
   
    println("Cerro?bf: ", sc.isStopped)
    sc.stop()
    println("Cerro?af: ", sc.isStopped)
 

  }

  object MyFunctions {
    def func1(s: Int): Unit = {
      var num: Int = 0
      num = s * 2
      //return num
    }
  }
}