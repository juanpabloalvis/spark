import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql._

import akka.actor.Actor

class ActorReadFile extends Actor {
  var isFirstTime: Boolean = false
  var numOfElements : Long = -1;
  //cuando extendemos de actor, debemos redefinir el metodo receive
  def receive = {
    case "tSpark1" =>
      println("Tarea1.Spark1. Mensaje de respuesta desde el actor Spark1, su estado es: ", isFirstTime)
      isFirstTime = true;
      
    case "tSpark2" =>
      println("Tarea2.a. Mensaje de respuesta en el actor. antes:", isFirstTime)
      val conf = new SparkConf().setAppName("Aplicaci�n con Spark y Akka") // Para pruebas unitarias podemos crear el objeto as*i*new SparkConf(false)
      // Set master, es la URL del cluster, que es Mesos, Yarn o Spark.
      conf.setMaster("local[5]")
      // Serializar el contexto spark  
      conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      // Contexto spark
      val sc = new SparkContext(conf)
      // Lee archivo csv
      val path = "..\\UnitTest\\airline-twitter-sentiment\\Tweets.csv";
      // Filtramos
      val input = sc.textFile(path, 4)
      val filteredInput = input.filter { _.contains("America") }
      // Se envia este objeto al cache de spark
      filteredInput.cache();  
      println("Numero de lineas archivo filtrado, desde el cache: ", filteredInput.count())
      filteredInput.take(100).foreach(println)
      
      println("Fin")
      numOfElements = filteredInput.count()      
    case "tareaContarEnSpark" =>
      // Esta linea es la m�s importante porque envia la respuesta de regreso a 
      // quien origino la peticion
      sender ! numOfElements 
        
    case msg => unhandled(msg)
  }

}